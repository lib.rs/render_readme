use syntect::parsing::*;

fn main() {
    let mut ss = SyntaxSetBuilder::new();
    ss.add_plain_text_syntax();
    ss.add_from_folder("../syntaxes", true).expect("syntaxes");
    let s = ss.build();

    syntect::dumps::dump_to_file(&s, "../syntax.dump").expect("syntax.dump");
}
