# Render README files the same way(ish) as GitHub et al.

Readme to HTML conversion for lib.rs. It's mostly internal,

Supports:

 * Markdown (natively in Rust) and
 * reStructuredText (via `rst2html`),
 * Supports syntax highlighting using Sublime Text syntax definitions.
   * Only generates `<span class>`. Actual theme colors are in the [style subproject](https://gitlab.com/lib.rs/style).
 * Has twreaks and hacks for Rust language and Rust crates, so e.g. falls back to Rust syntax if it can't detect the language used.

## Installation

Install `docutils` package, so that `rst2html` command is available (in `PATH`).

It needs to write a file to system's temp directory for `rst2html`. If you clear temp while the program is running, rst support may fail.

## Making a theme

It generates HTML `<span>` class names based on scope names from Sublime Syntax files, but:

 * words used in the scopes are abbreviated. See `highlight.rs` for the list of abbreviations.
 * Only scopes with 2 and 3 levels are added (more specific scopes are truncated to the first 3 words).

## Adding more languages

All languages are precompiled from `syntaxes/` submodule.

`tmLanguage` definitions need to be [converted first](https://github.com/aziz/SublimeSyntaxConvertor).
